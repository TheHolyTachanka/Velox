# Velox
A 🚀 blazingly fast 🚀 multi-threaded init system in 🦀 rust 🦀

### Features
- Memory-safe 🔨
    - Thanks to the modern rust compiler and borrow checker
- Blazingly fast ⏭️
    - Thanks to the rust compiler, rayon and asynchronous/multi-threaded code 

### Goals
- Be the fastest init system
- Use as much multi-threaded/asynchronous code(if it doesnt slow down the init)

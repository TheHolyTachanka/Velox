use bincode::config;
use crossbeam::channel;
use std::collections::HashMap;
use std::io::Read;
use std::os::unix::net::{UnixListener, UnixStream};
use std::os::unix::process::ExitStatusExt;

use std::sync::{Arc, Mutex};
use std::thread;
use std::{default, fs, path::PathBuf};

// Define a struct called Service
struct Service {
    data: velox::ServiceFile, // Store service data
}

impl Service {
    // Define a method to start the service and return a ServiceThread
    fn start(self) -> ServiceThread {
        let (send, commands) = channel::unbounded(); // Create a channel for communication between threads
        let process_info = Arc::new(Mutex::new(velox::ServiceStatus::Down)); // Create a shared mutable state for process status
        let process = process_info.clone(); // Clone the Arc to move into the thread

        thread::spawn(move || {
            let mut want_up = true; // Flag to indicate if the service should be started

            loop {
                if want_up {
                    // Start service
                    let mut process = self
                        .data
                        .commands
                        .get("start")
                        .expect("Start command required")
                        .spawn()
                        .unwrap();

                    *process_info.lock().unwrap() = velox::ServiceStatus::Running(process.id()); // Update the process status to running

                    // If oneshot, service is no longer wanted up
                    want_up = !self.data.service.oneshot;

                    // Wait for process to exit
                    let wait = process.wait().unwrap();

                    let exit_status = if let Some(code) = wait.code() {
                        velox::ExitStatus::Code(code as u8)
                    } else if let Some(signal) = wait.signal() {
                        velox::ExitStatus::Signal(signal)
                    } else {
                        unreachable!()
                    };

                    *process_info.lock().unwrap() = velox::ServiceStatus::Crashed(exit_status); // Update the process status to crashed

                    // Check for message from main thread
                    if let Ok(command) = commands.try_recv() {
                        use velox::ServiceCommands;

                        match command {
                            ServiceCommands::Down | ServiceCommands::Kill => {
                                want_up = false;

                                *process_info.lock().unwrap() = velox::ServiceStatus::Down; // Update the process status to down
                            }
                            ServiceCommands::Up | ServiceCommands::Restart => (), // Already up
                            ServiceCommands::Status => unreachable!(),
                        }
                    }
                } else {
                    // Check for message from main thread
                    if let Ok(command) = commands.recv() {
                        use velox::ServiceCommands;

                        match command {
                            ServiceCommands::Down | ServiceCommands::Kill => (), // Already down
                            ServiceCommands::Up | ServiceCommands::Restart => want_up = true,
                            ServiceCommands::Status => unreachable!(),
                        }
                    }
                }
            }
        });

        ServiceThread { process, send }
    }
}

impl From<velox::ServiceFile> for Service {
    fn from(data: velox::ServiceFile) -> Self {
        Self { data }
    }
}

// Define a struct called ServiceThread
struct ServiceThread {
    process: Arc<Mutex<velox::ServiceStatus>>, // Shared mutable state for process status
    send: channel::Sender<velox::ServiceCommands>, // Channel sender to communicate with the service thread
}

// Define a struct called ServiceManager
struct ServiceManager {
    services_dir: PathBuf, // Path to the directory containing service files
    running_services: HashMap<String, ServiceThread>, // Map of service names to their respective threads
    services: HashMap<String, Service>, // Map of service names to their respective Service instances
    services_provides: HashMap<String, Vec<String>>, // Map of provided service names to the services that provide them
}

impl ServiceManager {
    // Scan the service directory and populate the services and services_provides maps
    fn scan_service_dir(&mut self) {
        let service_paths = fs::read_dir(self.services_dir.as_path()).unwrap();

        // Setup service names for starting later
        for service_path in service_paths {
            let service_path = service_path.unwrap().path();

            let mut file_data = String::new();

            fs::File::open(&service_path)
                .unwrap()
                .read_to_string(&mut file_data)
                .unwrap();

            let service: velox::ServiceFile = toml::from_str(&file_data).unwrap();

            // Get name
            let mut name = service_path
                .file_stem()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string();

            if let Some(n) = &service.service.name {
                name = n.to_string()
            }

            let provides = service.service.provides.clone();

            self.services
                .insert(name.to_string(), Service::from(service));

            if let Some(provides) = provides {
                if let Some(names) = self.services_provides.get_mut(&provides) {
                    names.push(name.to_string());
                } else {
                    self.services_provides
                        .insert(provides, vec![name.to_string()]);
                }
            }
        }
    }

    // Start all services
    fn start(&mut self) {
        for service in self.services.drain() {
            let (name, service) = service;

            self.running_services.insert(name, service.start());
        }
    }

    // Handle a client connection
    fn handle_client(&mut self, mut stream: UnixStream) {
        // Read and decode message
        let message: velox::SockMessage =
            bincode::decode_from_std_read(&mut stream, config::standard())
                .expect("Failed to decode message");

        let service = self.running_services.get(&message.service).unwrap();

        if message.command != velox::ServiceCommands::Status {
            let signal = if message.command == velox::ServiceCommands::Kill {
                nix::sys::signal::SIGKILL
            } else {
                nix::sys::signal::SIGTERM
            };

            service.send.send(message.command).unwrap(); // Send the command to the service thread

            // Make sure program is killed
            let process = *service.process.lock().unwrap();

            if let velox::ServiceStatus::Running(pid) = process {
                if pid != 0 {
                    nix::sys::signal::kill(nix::unistd::Pid::from_raw(pid as i32), signal).unwrap();
                }
            }
        }

        // Let process start
        thread::sleep(std::time::Duration::from_millis(1));

        let process = *service.process.lock().unwrap();
        bincode::encode_into_std_write(process, &mut stream, config::standard()).unwrap(); // Encode the process status and send it back
    }

    // Initialize the service manager
    fn init(&mut self) {
        // Read all services
        self.scan_service_dir();

        // Start all services
        self.start();

        // Start listening to sock
        let listener = UnixListener::bind(velox::SOCK_LOCATION).unwrap();

        for stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    self.handle_client(stream);
                }
                Err(err) => {
                    panic!("{}", err)
                }
            }
        }
    }
}

impl default::Default for ServiceManager {
    fn default() -> Self {
        Self {
            running_services: HashMap::new(),
            services_dir: PathBuf::from("services/"),
            services: HashMap::new(),
            services_provides: HashMap::new(),
        }
    }
}

fn main() {
    ServiceManager::default().init(); // Create a ServiceManager instance and initialize it
}


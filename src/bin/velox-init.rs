use nix::sys::signal::{self, SigSet};
use signal_hook::consts::signal::*;
use signal_hook::iterator::Signals;
use std::env;

const VERSION: &str = env!("CARGO_PKG_VERSION"); // Constant for the version of the program
const SIGNALS: [i32; 5] = [SIGUSR1, SIGUSR2, SIGCHLD, SIGALRM, SIGINT]; // Array of signals to be handled
const TIME: u32 = 30; // Time interval for alarm signal

// Wrapper for reboot system call
unsafe fn sys_reboot(cmd: u32) -> Result<(), nc::Errno> {
    nc::reboot(nc::LINUX_REBOOT_MAGIC1, nc::LINUX_REBOOT_MAGIC2, cmd, 0) // Call to reboot system
}

#[derive(Clone, Copy)]
enum ShutdownType {
    PowerOff, // Power off the system
    Reboot, // Reboot the system
    CtrlAltDelete, // Send Ctrl-Alt-Delete signal
}

enum Stage {
    Start, // Start stage
    System, // System stage
    Shutdown(ShutdownType), // Shutdown stage with the specified shutdown type
}

fn spawn_stage(stage: Stage) -> std::process::Child {
    let file = match stage {
        Stage::Start => "/etc/velox/start",
        Stage::System => "/etc/velox/service",
        Stage::Shutdown(_) => "/etc/velox/shutdown",
    }; // Get the appropriate file path based on the stage

    let mut command = std::process::Command::new(file); // Create a new command for spawning a process

    if let Stage::Shutdown(t) = stage {
        command.arg(match t {
            ShutdownType::PowerOff => "poweroff",
            ShutdownType::Reboot => "reboot",
            ShutdownType::CtrlAltDelete => "ctrlaltdelete",
        }); // Add an argument to the command based on the shutdown type
    }

    command.spawn().unwrap() // Spawn the process and return the child
}

fn startup() -> bool {
    let mut child = spawn_stage(Stage::Start); // Spawn the start stage

    // Block all signals
    let sigset = SigSet::all(); // Create a signal set containing all signals
    signal::sigprocmask(signal::SigmaskHow::SIG_BLOCK, Some(&sigset), None).unwrap(); // Block the signals in the set

    // Check return code of startup
    let ret = match child.wait().unwrap().code() {
        Some(code) => code == 111,
        None => false,
    }; // Check the return code of the child process

    // Unblock all signals
    signal::sigprocmask(signal::SigmaskHow::SIG_UNBLOCK, Some(&sigset), None).unwrap(); // Unblock the signals in the set

    ret // Return the result of startup
}

fn shutdown(t: ShutdownType) {
    spawn_stage(Stage::Shutdown(t)); // Spawn the shutdown stage

    let reboot_cmd = match t {
        ShutdownType::PowerOff => nc::LINUX_REBOOT_CMD_POWER_OFF,
        ShutdownType::Reboot | ShutdownType::CtrlAltDelete => nc::LINUX_REBOOT_CMD_RESTART,
    }; // Determine the appropriate reboot command based on the shutdown type

    unsafe {
        nc::sync().unwrap(); // Synchronize all file data
        sys_reboot(reboot_cmd).unwrap(); // Reboot the system with the specified command
    }
}

fn init() -> ! {
    if std::process::id() != 1 {
        panic!("Unable to start init - must be PID 1"); // Check if the process is running as PID 1 (init process)
    }

    // Spawn first init script
    if !startup() {
        // Startup failed - shutdown
        shutdown(ShutdownType::PowerOff); // Perform power off if startup failed
    }

    // Install handler
    let mut signals = Signals::new(&SIGNALS).unwrap(); // Create a signal handler for the specified signals

    // Disable kernel Ctrl-Alt-Delete and send SIGINT's to this process
    unsafe { sys_reboot(nc::LINUX_REBOOT_CMD_CAD_OFF) }.unwrap(); // Disable kernel Ctrl-Alt-Delete and send SIGINTs to this process

    // Spawn services
    spawn_stage(Stage::System); // Spawn the system stage

    // Wait for signals
    loop {
        for signal in signals.pending() {
            match signal {
                // Spawn shutdown stage
                SIGUSR1 => shutdown(ShutdownType::PowerOff), // Handle SIGUSR1 signal - power off the system
                SIGUSR2 => shutdown(ShutdownType::Reboot), // Handle SIGUSR2 signal - reboot the system
                SIGINT => shutdown(ShutdownType::CtrlAltDelete), // Handle SIGINT signal - send Ctrl-Alt-Delete signal

                // Handle children
                SIGALRM | SIGCHLD => {
                    nix::sys::wait::waitpid(
                        nix::unistd::Pid::from_raw(-1),
                        Some(nix::sys::wait::WaitPidFlag::WNOHANG),
                    )
                    .unwrap(); // Wait for child processes

                    unsafe {
                        _ = nc::alarm(TIME); // Set an alarm to be triggered after the specified time interval
                    }
                }

                _ => unreachable!(),
            }
        }
    }
}

fn main() {
    let mut args = env::args();

    match args.nth(1) {
        Some(s) => match s.as_str() {
            "-v" => println!("Velox version: {}", VERSION), // Print the version of the program
            _ => println!("Usage: velox-init [-v]"), // Print the usage information
        },
        None => init(), // Run the init process
    }
}


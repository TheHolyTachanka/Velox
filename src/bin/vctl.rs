use bincode::config;
use std::os::unix::net::UnixStream;

fn command_from_args() -> velox::SockMessage {
    let mut args = std::env::args();

    let command = args.nth(1).unwrap_or_else(|| {
        print_help();
        std::process::exit(1);

    });
    if command == "--help" {
        print_help();
        std::process::exit(0);
    }

    let service = args.next().expect("Provide a service");

    let command = match command.as_str() {
        "down" => velox::ServiceCommands::Down, // Sends term
        "kill" => velox::ServiceCommands::Kill, // Sends kill
        "up" => velox::ServiceCommands::Up,
        "restart" => velox::ServiceCommands::Restart, // Sends term
        "status" => velox::ServiceCommands::Status,

        _ => panic!("Unknown command"),
    };

    velox::SockMessage { service, command }
}

fn print_help() {
    println!("Usage: vctl [COMMAND] [SERVICE]");
    println!("Available commands:");
    println!("  down    - Stop the service gracefully");
    println!("  kill    - Forcefully kill the service");
    println!("  up      - Start the service");
    println!("  restart - Restart the service gracefully");
    println!("  status  - Check the status of the service");
}

fn main() {
    let message = command_from_args();
    let mut stream = UnixStream::connect(velox::SOCK_LOCATION).unwrap();

    bincode::encode_into_std_write(&message, &mut stream, config::standard()).unwrap();

    let data: velox::ServiceStatus =
        bincode::decode_from_std_read(&mut stream, config::standard()).unwrap();

    println!("{:?}", data);
}

